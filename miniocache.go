// Package miniocache provides an implementation of httpcache.Cache that stores and
// retrieves data using Minio.
package miniocache

import (
	"bytes"
	"compress/gzip"
	"context"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"io"
	"log"

	"github.com/minio/minio-go/v7"
)

// Cache objects store and retrieve data using Minio Client.
type Cache struct {
	Client *minio.Client

	Bucket string

	Compression Compression

	LogErrors bool
}

type Compression interface {
	NewGetReader(value io.Reader) (reader io.Reader, err error)
	NewPutReader(resp []byte) (reader io.Reader, length int, err error)
	Extension() string
}

var Gzip = NewCompression(func(in io.Reader) (out io.Reader, err error) {
	return gzip.NewReader(in)
}, func(w io.Writer) (io.WriteCloser, error) {
	return gzip.NewWriter(w), nil
}, "gz")

func (c *Cache) Get(key string) (resp []byte, ok bool) {
	// obj is lazy and doesn't return not exists error until you try reading from it
	obj, err := c.Client.GetObject(context.Background(), c.Bucket, c.url(key), minio.GetObjectOptions{})
	if err != nil {
		if c.LogErrors {
			log.Printf("miniocache.Get failed: %s", err)
		}
		return []byte{}, false
	}
	defer obj.Close()

	r := io.Reader(obj)
	if c.Compression != nil {
		r, err = c.Compression.NewGetReader(r)
	}

	if err == nil {
		resp, err = io.ReadAll(r)
	}

	if err != nil {
		if c.LogErrors && !isErrorNoSuchKey(err) {
			log.Printf("miniocache.Get failed: %s", err)
		}
		return []byte{}, false
	}
	return resp, err == nil
}

func isErrorNoSuchKey(err error) bool {
	var merr minio.ErrorResponse
	if errors.As(err, &merr) {
		return merr.Code == "NoSuchKey"
	}
	return false
}

func (c *Cache) Set(key string, resp []byte) {
	var r io.Reader
	var l int
	var err error

	if c.Compression != nil {
		r, l, err = c.Compression.NewPutReader(resp)
		if err != nil {
			if c.LogErrors {
				log.Printf("miniocache.Set failed: %s", err)
			}
			return
		}
	} else {
		r = bytes.NewReader(resp)
		l = len(resp)
	}

	_, err = c.Client.PutObject(context.Background(), c.Bucket, c.url(key), r, int64(l), minio.PutObjectOptions{})
	if err != nil {
		if c.LogErrors {
			log.Printf("miniocache.Set failed: %s", err)
		}
		return
	}
}

func (c *Cache) Delete(key string) {
	err := c.Client.RemoveObject(context.Background(), c.Bucket, c.url(key), minio.RemoveObjectOptions{})
	if err != nil {
		if c.LogErrors {
			log.Printf("miniocache.Delete failed: %s", err)
		}
		return
	}
}

func (c *Cache) url(key string) string {
	key = cacheKeyToObjectKey(key)
	if c.Compression != nil {
		return key + "." + c.Compression.Extension()
	}
	return key
}

func cacheKeyToObjectKey(key string) string {
	h := md5.New()
	io.WriteString(h, key)
	return hex.EncodeToString(h.Sum(nil))
}

type compression struct {
	newReader func(io.Reader) (io.Reader, error)
	newWriter func(io.Writer) (io.WriteCloser, error)
	extension string
}

func NewCompression(newReader func(io.Reader) (io.Reader, error), newWriter func(io.Writer) (io.WriteCloser, error), extension string) Compression {
	return &compression{newReader, newWriter, extension}
}

func (c *compression) NewGetReader(r io.Reader) (io.Reader, error) {
	return c.newReader(r)
}

func (c *compression) NewPutReader(resp []byte) (_ io.Reader, _ int, err error) {
	b := &bytes.Buffer{}
	var w io.WriteCloser
	if w, err = c.newWriter(b); err != nil {
		return
	}
	if _, err = w.Write(resp); err != nil {
		return
	}
	if err = w.Close(); err != nil {
		return
	}
	return b, b.Len(), nil
}

func (c *compression) Extension() string {
	return c.extension
}
