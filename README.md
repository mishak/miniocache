# Minio Cache

Cache Backend for [github.com/gregjones/httpcache](https://github.com/gregjones/httpcache)

```go
// setup minio client
client, err := minio.New(os.Getenv("MINIO_ENDPOINT"), &minio.Options{
    Creds:     credentials.NewStaticV4(os.Getenv("MINIO_ACCOUNT_KEY"), os.Getenv("MINIO_SECRET_KEY"), ""),
    // ...
})
if err != nil {
    panic(err)
}

// cache http requests
http.DefaultTransport = httpcache.NewTransport(&miniocache.Cache{
    Client:      client,
    Bucket:      "my-http-cache",

    // enable compression (optional)
    Compression: miniocache.Gzip,
})
```

## Alternative Compressions

Besides gzip you can use other compression algorithms

```golang
&miniocache.Cache{
    // ...

    // github.com/klauspost/compress/s2
	Compression: NewCompression(func(r io.Reader) (io.Reader, error) {
        return s2.NewReader(r), nil
    }, func(w io.Writer) (io.WriteCloser, error) {
        return s2.NewWriter(w, s2.WriterBestCompression()), nil
    }, "s2"),

    // github.com/ulikunitz/xz
	Compression: NewCompression(func(r io.Reader) (io.Reader, error) {
		return xz.NewReader(r)
	}, func(w io.Writer) (io.WriteCloser, error) {
		return xz.NewWriter(w)
	}, "xz"),

    // github.com/klauspost/compress/zstd
	Compression: NewCompression(func(r io.Reader) (io.Reader, error) {
        return zstd.NewReader(r)
    }, func(w io.Writer) (io.WriteCloser, error) {
        return zstd.NewWriter(w, zstd.WithEncoderLevel(zstd.SpeedBestCompression))
    }, "zst"),
}
```
